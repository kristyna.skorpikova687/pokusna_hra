using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pond : MonoBehaviour
{
    private GameObject player;
    public GameObject bucket;
    public GameObject water;
    private bool nearby;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("player");
        water.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        bool inventar = bucket.GetComponent<Coin>().use;
        if(nearby && inventar)
        {
            bucket.SetActive(false);
            water.SetActive(true);
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = true;
        }


    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = false;
        }
    }
}
