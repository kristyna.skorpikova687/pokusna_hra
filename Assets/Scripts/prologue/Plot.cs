using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plot : MonoBehaviour
{
    private GameObject player;
    public GameObject sword;
    private bool inventar;
    private bool nearby;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("player");
    }

    // Update is called once per frame
    void Update()
    {
        inventar = sword.GetComponent<Coin>().use;
        if (nearby && inventar)
        {
            gameObject.SetActive(false);
        }
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = true;
        }


    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = false;
        }
    }
}
