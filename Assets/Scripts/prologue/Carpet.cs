using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carpet : MonoBehaviour
{
    private GameObject player;
    private bool nearby;
    public GameObject pot;
    public GameObject seed;
    public GameObject water;
    public GameObject bucket;
    public GameObject potplant;
    public GameObject plant;
    private bool potted;
    private bool watered;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("player");
        potplant.SetActive(false);
        plant.SetActive(false);
        potted = false;
        watered = false;
    }

    // Update is called once per frame
    void Update()
    {
        Coin inventar = pot.GetComponent<Coin>();
        if(nearby && inventar.use)
        {
            potted = true;
            inventar.enabled = false;
            pot.transform.position = transform.position;
            UnityEngine.Debug.Log("pot");
        }
        bool inventars = seed.GetComponent<Coin>().use;       
        if (nearby && inventars && potted)
        {
            watered = true;
            pot.SetActive(false);
            seed.SetActive(false);
            potplant.SetActive(true);
            UnityEngine.Debug.Log("Potted");
        }
        bool inventarw = water.GetComponent<Coin>().use;
        if (nearby && inventarw && watered)
        {
            potplant.SetActive(false);
            water.SetActive(false);
            plant.SetActive(true);
            bucket.SetActive(true);    
            UnityEngine.Debug.Log("Wattered");

        }

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = true;           
        }


    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = false;
        }
    }
}
