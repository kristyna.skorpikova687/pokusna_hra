using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour
{
    public GameObject player;
    private bool nearby = false;
    public Transform target;
    public Vector3 offset;
    private bool obtained = false;
    public GameObject npc;
    [HideInInspector]
    public bool inventar;
    // Start is called before the first frame update
    void Start()
    {
        inventar = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        obtained = npc.GetComponent<Quest>().koupeno;
        if (nearby && Input.GetKey(KeyCode.A) && obtained)
        {            
            inventar = true;
            UnityEngine.Debug.Log("Sword collected!");
        }
        if (inventar)
        {
            transform.position = target.position + offset;
        }
        /*if (Input.GetKeyDown(KeyCode.D) && inventar)
        {
            transform.position = target.position;
            inventar = false;
            
        }
        */
        if (obtained == false)
        {
            inventar = false;
        }

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = true;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = false;
        }
    }
}
