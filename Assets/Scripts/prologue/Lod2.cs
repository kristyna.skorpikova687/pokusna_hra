using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Lod2 : MonoBehaviour
{
    private GameObject player;
    public GameObject lod1;
    public Vector2 konec;
    public Vector3 lod;
    public Vector3 konecna;
    public float speed;
    private Portal portal;
    private Portal portal1;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("player");
        portal = GetComponent<Portal>();
        portal1 = lod1.GetComponent<Portal>();
        portal.enabled = false;
        portal1.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        Pohyb pohyb = player.GetComponent<Pohyb>();
        pohyb.enabled = false;        
        player.transform.position = lod;
        player.transform.position = Vector2.MoveTowards(transform.position, konec, speed * Time.deltaTime);
        transform.position = Vector2.MoveTowards(transform.position, konec, speed * Time.deltaTime);

        if ((Vector2)transform.position == konec)
        {
            player.transform.position = konecna;
            if (Input.GetKey(KeyCode.E))
            {
                SceneManager.LoadScene("Desert");

            }
        }
        
        
    }
    
}
