using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Majak : MonoBehaviour
{
    private GameObject player;
    public GameObject svetlo;
    public GameObject majak;
    private bool nearby;
    public GameObject lod;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("player");
        majak.SetActive(false);
        lod.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        bool inventar = svetlo.GetComponent<Coin>().use;
        if (nearby && inventar)
        {
            gameObject.SetActive(false);
            majak.SetActive(true);
            svetlo.SetActive(false);
            lod.SetActive(true);
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = true;
        }


    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = false;
        }
    }
}
