using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Security.Permissions;
using UnityEngine;

public class Krab : MonoBehaviour
{
    public Vector2 koncovaPozice;
    private Vector2 zacatecniPozice;
    private GameObject player;
    public float speed;
    public GameObject portal;
    private bool entered;
    private bool tam;
    public GameObject sword;
    private bool WillIGetKilled;
    private bool killed;
    private Coin obtained;
    private bool nearby = false;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("player");
        entered = false;
        zacatecniPozice = transform.position;
        obtained = GetComponent<Coin>();
        obtained.enabled = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        WillIGetKilled = sword.GetComponent<Coin>().inventar;
        killed = sword.GetComponent<Coin>().use;
        entered = portal.GetComponent<Portal>().isThere;
        if(entered && killed == false) 
        {
            if(zacatecniPozice == (Vector2)transform.position) tam = true;
            else if(koncovaPozice == (Vector2)transform.position) tam = false;
            if (tam)
            {
                transform.position = Vector2.MoveTowards(transform.position, koncovaPozice, speed * Time.deltaTime);
            }
            else 
            {
                transform.position = Vector2.MoveTowards(transform.position, zacatecniPozice, speed * Time.deltaTime);
            }
       
        }        
        if(nearby && WillIGetKilled == false) 
        {
            Smrt dead = player.GetComponent<Smrt>();
            dead.OnPlayerDeath();
        }
        else if(nearby && WillIGetKilled) 
        {
            if (killed) 
            {                             
                obtained.enabled = true;
                enabled = false;
            }
            
        }

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = true;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = false;
        }
    }
}
