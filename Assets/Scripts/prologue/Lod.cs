using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Lod : MonoBehaviour
{
    public GameObject lod;
    public Vector2 konec;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        lod.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, konec, speed * Time.deltaTime);
        if ((Vector2)transform.position == konec && Input.GetKey(KeyCode.E))
        {
            lod.SetActive(true);
            gameObject.SetActive(false);
            Lod lod1 = GetComponent<Lod>();
            lod1.enabled = false;
        }
    }
}
