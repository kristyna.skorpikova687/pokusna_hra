using System.Collections;
using System.Collections.Generic;
using System.Runtime;
using UnityEngine;

public class Lock : MonoBehaviour
{
    public GameObject key;
    private GameObject player;
    public GameObject door;
    private bool haveKey;
    private bool nearby;
    private CapsuleCollider2D colider;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("player");
        colider = door.GetComponent<CapsuleCollider2D>();
        colider.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        haveKey = key.GetComponent<Coin>().use;
        if (nearby && haveKey)
        {           
            colider.enabled = true;
            key.SetActive(false);
            gameObject.SetActive(false);
        }

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = true;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = false;
        }
    }
}
