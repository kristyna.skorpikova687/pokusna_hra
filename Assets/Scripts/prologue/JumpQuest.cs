using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpQuest : MonoBehaviour
{
    private GameObject player;
    private Jumping jump;
    private Pohyb pohyb;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("player");
        jump = player.GetComponent<Jumping>();
        pohyb = player.GetComponent<Pohyb>();
    }

    // Update is called once per frame
    void Update()
    {
        bool isThere = GetComponent<Portal>().isThere;
        if (isThere) 
        {
            jump.enabled= true;
            pohyb.enabled= false;
        }
        else
        {
            jump.enabled= false;
            pohyb.enabled= true;
        }
        
    }
}
