using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : MonoBehaviour
{
    public Vector2 koncovaPozice;
    private Vector2 zacatecniPozice;
    private GameObject player;
    public float speed;
    public GameObject portal;
    private bool entered;
    private bool tam;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("player");
        entered = false;
        zacatecniPozice = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        entered = portal.GetComponent<Portal>().isThere;
        if (entered)
        {
            if (zacatecniPozice == (Vector2)transform.position) tam = true;
            else if (koncovaPozice == (Vector2)transform.position) tam = false;
            if (tam)
            {
                transform.position = Vector2.MoveTowards(transform.position, koncovaPozice, speed * Time.deltaTime);
            }
            else
            {
                transform.position = Vector2.MoveTowards(transform.position, zacatecniPozice, speed * Time.deltaTime);
            }

        }

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            Smrt dead = player.GetComponent<Smrt>();
            dead.OnPlayerDeath();
        }
    }
    
}
