using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventarPosition : MonoBehaviour
{
    
    private Camera mainCamera;
    private GameObject player;
    public float offsetRight;
    public float offsetTop; // Optional offsetRightCorner from the corner
    private Vector3 offsetPlayer;

    private void Start()
    {
        mainCamera = Camera.main;
        player = GameObject.FindWithTag("player");
        mainCamera.transform.position = player.transform.position;
        GenerateObjectPosition();
    }
    // Update is called once per frame
    void Update()
    {
        transform.position = player.transform.position + offsetPlayer;
    }

    private void GenerateObjectPosition()
    {
        // Get the screen's aspect ratio
        float screenAspect = (float)Screen.width / Screen.height;

        // Calculate the world coordinates of the top-right corner of the screen
        float cameraHeight = mainCamera.orthographicSize * 2f;
        float cameraWidth = cameraHeight * screenAspect;
        float xPos = (mainCamera.transform.position.x + cameraWidth / 2f - offsetRight) - player.transform.position.x;
        float yPos = (mainCamera.transform.position.y + cameraHeight / 2f - offsetTop) - player.transform.position.y;
        offsetPlayer = new Vector3 (xPos, yPos, 0);
        
    }
}

