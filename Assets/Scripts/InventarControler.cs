using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class InventarControler : MonoBehaviour
{
    public GameObject[] slots;
    public bool[] freeSlot;
    // Start is called before the first frame update
    void Start()
    {
        slots = GameObject.FindGameObjectsWithTag("slot");       
        freeSlot = new bool[slots.Length];
        for(int i = 0; i < freeSlot.Length; i++)
        {
            freeSlot[i] = true;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public GameObject FindUnoccupiedSlot()
    {
        for (int i = 0; i < freeSlot.Length; i++)
        {
            if (freeSlot[i])
            {
                freeSlot[i] = false;
                UnityEngine.Debug.Log(i+""+ freeSlot[i]);
                return slots[i];
            }
        }

        return null; 
    }
}
