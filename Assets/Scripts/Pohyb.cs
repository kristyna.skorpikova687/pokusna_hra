using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

public class Pohyb : MonoBehaviour
{  
    private Rigidbody2D rbs;

    // Start is called before the first frame update
    void Start()
    {
        rbs = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        rbs.gravityScale = 0;
        rbs.drag = 1;
        if (Input.GetKey(KeyCode.LeftArrow))
            rbs.AddForce(new Vector2(-4,0));
        if (Input.GetKey(KeyCode.RightArrow))
            rbs.AddForce(new Vector2(4, 0));
        if (Input.GetKey(KeyCode.UpArrow))
            rbs.AddForce(new Vector2(0, 4));
        if (Input.GetKey(KeyCode.DownArrow))
            rbs.AddForce(new Vector2(0, -4));
    }
}
