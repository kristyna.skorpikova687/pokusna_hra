using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Cryptography;
using UnityEngine;
using UnityEditor;
using System.Collections.Specialized;
using System.Runtime.InteropServices;
using System;
using System.Net.Mime;
using Unity.Mathematics;

public class Coin : MonoBehaviour
{
    private bool nearby;
    public GameObject player;
    private GameObject backpack;
    private GameObject slot;
    public bool swap;
    public bool turn;
    public bool zvetsit;
    public bool inventar;
    public bool inUsingSlot;
    public Quaternion rotation;
    public Vector2 scale;
    public bool use;
    private GameObject usingSlot;
    private UsingSlotPosition usingSlotPosition;
    private InventarControler controler;
    public Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("player");
        backpack = GameObject.FindWithTag("backpack");
        usingSlot = GameObject.FindWithTag("usingSlot");
        inventar = false;
        use = false;
        inUsingSlot = false;
        usingSlotPosition = usingSlot.GetComponent<UsingSlotPosition>();
        controler = backpack.GetComponent<InventarControler>();
    }

    // Update is called once per frame
    void Update()
    {
        if (swap) inventar = true;
        if (nearby && Input.GetKeyDown(KeyCode.A))
        {
            UnityEngine.Debug.Log("Item collected!");
            inventar = true;
            Renderer objectRenderer;
            objectRenderer = GetComponent<Renderer>();
            objectRenderer.sortingOrder = 7;

            if (turn)
            {               
                transform.rotation = rotation;
            }
            if (zvetsit)
            {               
                transform.localScale = scale;
            }
            slot = controler.FindUnoccupiedSlot();
            offset = player.transform.position - slot.transform.position;
        }       
        if(inventar) 
        {
            transform.position = player.transform.position - offset;
        }
        if(Input.GetKey(KeyCode.U))
        {
            double mathDistance = 0.01;
            if (inventar && mathDistance > math.distance(usingSlot.transform.position, transform.position))
            {
                use = true;                        
            }
        }
        else use = false;      

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = true;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = false;
        }
    }
    void OnMouseDown()
    {
        if (inventar && usingSlotPosition.usingObject == null)
        {
            controler.freeSlot[System.Array.IndexOf(controler.slots, slot)] = true;
            usingSlotPosition.usingObject = gameObject;
            offset = player.transform.position - usingSlot.transform.position;
        }
        else if (inventar && usingSlotPosition.usingObject!=null)
        {
            Coin itemsInUsingSlot = usingSlotPosition.usingObject.GetComponent<Coin>();
            itemsInUsingSlot.offset = offset;
            offset = player.transform.position - usingSlot.transform.position;
            usingSlotPosition.usingObject = gameObject;
        }
    }
    
    
}
[CustomEditor(typeof(Coin))]
public class CoinEditor : Editor
{
    public override void OnInspectorGUI()
    {
        Coin coin = (Coin)target;

        coin.turn = EditorGUILayout.Toggle("Turn",coin.turn);
        if (coin.turn)
        {
            coin.rotation = Quaternion.Euler(EditorGUILayout.Vector3Field("Rotation",coin.rotation.eulerAngles));
        }
        coin.zvetsit = EditorGUILayout.Toggle("Zvetsit",coin.zvetsit);
        if (coin.zvetsit)
        {
            coin.scale = EditorGUILayout.Vector2Field("Scale",coin.scale);
        }
        coin.swap = EditorGUILayout.Toggle("Swap", coin.swap);               


    }
}
