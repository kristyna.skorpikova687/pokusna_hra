using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropastSmrt : MonoBehaviour
{
    private GameObject player;
    private bool nearby;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("player");
    }

    // Update is called once per frame
    void Update()
    {
        Smrt smrt = player.GetComponent<Smrt>();
        if (nearby) 
        {
            smrt.OnPlayerDeath();
        
        }
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = true;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = false;
        }
    }

}
