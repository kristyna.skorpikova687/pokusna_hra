using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainButton : MonoBehaviour
{
    private bool nearby;
    private GameObject player;
    public GameObject onButton;
    private GameObject[] items;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("player");
        onButton.SetActive(false);
        items= GameObject.FindGameObjectsWithTag("item");
        foreach (GameObject obj in items)
        {
            obj.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(nearby && Input.GetKey(KeyCode.T))
        {
            onButton.SetActive(true);
            gameObject.SetActive(false);
            foreach (GameObject obj in items)
            {
                obj.SetActive(true);
            }
        }
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = true;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = false;
        }
    }

}
