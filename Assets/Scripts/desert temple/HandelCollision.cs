using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandelCollision : MonoBehaviour
{
    private GameObject player;
    [HideInInspector]
    public bool collision;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("player");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            collision = true;

        }

    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            collision = false;
        }
    }
}
