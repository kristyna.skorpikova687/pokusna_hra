using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravitace : MonoBehaviour
{
    private GameObject player;
    private Jumping jump;
    private Pohyb pohyb;
    private bool isThere;
    private int exit;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("player");
        jump = player.GetComponent<Jumping>();
        pohyb = player.GetComponent<Pohyb>();
        exit = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (isThere)
        {
            jump.enabled = true;
            pohyb.enabled = false;
        }
        else if (exit == 1)
        {
            jump.enabled = false;
            pohyb.enabled = true;
            exit = 0;
        }
      
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            isThere = true;
            exit = 0;

        }

    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            isThere = false;
            exit = 1;
        }
    }
}
