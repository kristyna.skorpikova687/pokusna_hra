using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rope : MonoBehaviour
{
    public GameObject rope;
    private GameObject[] buttons;
    private bool allActive;
    // Start is called before the first frame update
    void Start()
    {
        buttons = GameObject.FindGameObjectsWithTag("button");
        allActive = false;
        rope.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject obj in buttons)
        {
            if (!obj.activeSelf)
            {
                allActive = false;
                break;
            }
        }
        if (allActive) 
        {
            rope.SetActive(true);
            enabled = false;
        }
    }
}
