using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Delivery : MonoBehaviour
{
    private GameObject player;
    public GameObject item;
    public GameObject on;
    public GameObject off;
    private bool nearby;
    private Coin coin;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("player");
        on.SetActive(false);
        coin = item.GetComponent<Coin>();
    }

    // Update is called once per frame
    void Update()
    {
        if (nearby && coin.use)
        {
            on.SetActive(true);
            off.SetActive(false);
            item.transform.position = transform.position;
            coin.enabled = false;
            gameObject.SetActive(false);
            UnityEngine.Debug.Log("Item delivered!");

        }
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = true;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = false;
        }
    }
}
