using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumping : MonoBehaviour
{
    private Rigidbody2D rbs;
    private bool jump;
    // Start is called before the first frame update
    void Start()
    {
        rbs = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        rbs.gravityScale = 1;
        rbs.drag = 2;
        if (Input.GetKey(KeyCode.LeftArrow))
            rbs.AddForce(new Vector2(-4, 0));
        if (Input.GetKey(KeyCode.RightArrow))
            rbs.AddForce(new Vector2(4, 0));
        if (Input.GetKeyDown(KeyCode.Space) && jump)
        {
            rbs.AddForce(new Vector2(0, 800));
            jump = false;
        }
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        jump = true;
    }
}
