using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Diagnostics;

public class Portal : MonoBehaviour
{
    public GameObject player;
    private bool port = false;
    public GameObject end;
    public bool chodba;
    public GameObject chodbaEnter;
    public GameObject chodbaExit;
    public bool isThere;
    private bool portToTheEnd;
    private bool thisPort;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("player");
        isThere = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!chodba)
        {
            if (port && Input.GetKeyDown(KeyCode.E)&&chodba==false)
            {
                player.transform.position = end.transform.position;
                port = false;
                isThere = false;
            }

        }
        else if (chodba) 
        {
            portToTheEnd = chodbaExit.GetComponent<HandelCollision>().collision;
            if (port && Input.GetKeyDown(KeyCode.E))
            {
                player.transform.position = chodbaEnter.transform.position;
                port = false;
                thisPort = true;
            }
            if (portToTheEnd && Input.GetKeyDown(KeyCode.E) && thisPort)
            {
                player.transform.position = end.transform.position;
                portToTheEnd = false;
                thisPort=false;
            }
            
        }

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player) 
        {
            isThere = true; 
            port = true;
            UnityEngine.Debug.Log("Portal enter");
            
        } 
       
    }
    void OnTriggerExit2D(Collider2D other)
    {
       
        if (other.gameObject == player)
        {
            port = false;
            UnityEngine.Debug.Log("Portal exit");

        }
        
    }

}
[CustomEditor(typeof(Portal))]
public class PortalEditor : Editor
{
    public override void OnInspectorGUI()
    {
        Portal portal = (Portal)target;
      
        portal.end = (GameObject)EditorGUILayout.ObjectField("End", portal.end, typeof(GameObject), true);
        portal.chodba = EditorGUILayout.Toggle("Chodba", portal.chodba);
        if (portal.chodba)
        {
            portal.chodbaEnter = (GameObject)EditorGUILayout.ObjectField("Enter", portal.chodbaEnter, typeof(GameObject), true);
            portal.chodbaExit = (GameObject)EditorGUILayout.ObjectField("Exit", portal.chodbaExit, typeof(GameObject), true);
        }        

    }
}
