using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class Quest : MonoBehaviour
{
    private GameObject player;
    public GameObject coin;
    public GameObject sword;
    public GameObject rcoin;
    private bool nearby = false;
    private bool trade;
    private Coin item;
    [HideInInspector]
    public bool koupeno;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("player");
        koupeno = false;
        sword.SetActive(false);
        rcoin.SetActive(false);
        item = sword.GetComponent<Coin>();
        item.enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
        trade = coin.GetComponent<Coin>().use;        
        if (nearby && koupeno == false)
        {
            sword.SetActive(true);
            rcoin.SetActive(true);
        }
        else if (koupeno == false)
        {
            sword.SetActive(false);
            rcoin.SetActive(false);
        }
        if (nearby && trade)
        {
            coin.SetActive(false);
            rcoin.SetActive(false);
            koupeno = true;
            item.enabled = true;

        }

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = true;
            UnityEngine.Debug.Log("nearby");
        }


    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            nearby = false;
        }
    }

}
